import CounterApp from './components/CounterApp';
import { useSelector, useDispatch } from "react-redux";
import './App.css';

function App() {
  const state = useSelector(state => state)
  const counters = Object.keys(state)
  const dispatch = useDispatch()

  const allCounters = counters.map(eachCounter => {
    const count = state[eachCounter]
    return <CounterApp name={eachCounter} value={count} key={eachCounter} />
  })

  const addNewCounter = () => {
    const name = (`counter${counters.length + 1}`)
    dispatch({ type: 'addNewCounter', name})
  }

  return (
    <div className="App App-header">
      <h1>Counter App</h1>
      {allCounters}
      <button className="add-counter-btn" onClick={addNewCounter}>Add Counter</button>
    </div>
  );
}

export default App;
