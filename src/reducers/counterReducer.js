const counterReducer = (state = { counter1: 0 }, action) => {
    switch (action.type) {
        case "Increment":
            const incrementedState = { ...state }
            incrementedState[action.name] += 1
            return incrementedState
        case "Decrement":
            const decreamentedState = { ...state }
            decreamentedState[action.name] -= 1
            return decreamentedState
        case "addNewCounter":
            const newState = { ...state, [action.name]: 0 }
            return newState
        default:
            return state;
    }
}

export default counterReducer;