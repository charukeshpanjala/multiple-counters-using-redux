import { useDispatch } from "react-redux";
import "./index.css";

function CounterApp(props) {
    const { name, value } = props;
    const dispatch = useDispatch()

    const incrementCounter = () => {
        dispatch({ type: "Increment", name })
    }
    const decrementCounter = () => {
        dispatch({ type: "Decrement", name })
    }

    return (<div className="counter-container">
        <p className="counter-name">{name}</p>
        <h1 className="count">{value}</h1>
        <button className="btn" onClick={incrementCounter}>Increment</button>
        <button className="btn" onClick={decrementCounter}>Decrement</button>
    </div>);
}

export default CounterApp;